@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
        
        <div>ok</div>


        <ul class="nav nav-pills" role="tablist">
            
            <li class="nav-item"><a class="nav-link active" data-bs-toggle="pill" href="#F">f</a></li>
            <li class="nav-item"><a class="nav-link" data-bs-toggle="pill" href="#M">m</a></li>
            
        </ul>

        <div class="tab-content">
            <div id="F" class="container tab-pane active"><br>
                <ul class="nav nav-pills" role="tablist">
                    <li class="nav-item">
                    <a class="nav-link active" data-bs-toggle="pill" href="#homeF">Raspored</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" data-bs-toggle="pill" href="#menu1F">Rezultati</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" data-bs-toggle="pill" href="#menu2F">Grupe</a>
                    </li>
                </ul>
                
                <!-- Tab panes -->
                <div class="tab-content">
                    <div id="homeF" class="container tab-pane active"><br>
                    <h3>HOME</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    </div>

                    <div id="menu1F" class="container tab-pane fade"><br>
                    <h3>Menu 1</h3>
                    <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                    </div>

                    <div id="menu2F" class="container tab-pane fade"><br>
                    <h3>Menu 2</h3>
                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
                    </div>
                </div>
            </div>

            <div id="M" class="container tab-pane"><br>
                <ul class="nav nav-pills" role="tablist">
                    <li class="nav-item">
                    <a class="nav-link active" data-bs-toggle="pill" href="#homeM">Raspored</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" data-bs-toggle="pill" href="#menu1M">Rezultati</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" data-bs-toggle="pill" href="#menu2M">Grupe</a>
                    </li>
                </ul>
                
                <!-- Tab panes -->
                <div class="tab-content">
                    <div id="homeM" class="container tab-pane active"><br>
                    <h3>HOME</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    </div>

                    <div id="menu1M" class="container tab-pane fade"><br>
                    <h3>Menu 1</h3>
                    <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                    </div>

                    <div id="menu2M" class="container tab-pane fade"><br>
                    <h3>Menu 2</h3>
                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
                    </div>
                </div>
            </div>

        </div>


        </div>
    </div>
</div>
@endsection

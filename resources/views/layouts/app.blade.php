<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Scripts -->
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
    <script src="{{ mix('js/app.js') }}" defer></script>
    <style>
        .nav-pills > li > a.active {   background-color: #6c757d !important;   color: #fff !important; } 
        .nav-pills > li > a:hover {   background-color: #dee2e6 !important; color: #000 !important; }  
        .nav-link {   color: #000; } 

            
        /* unvisited link */
        a:link {
        color: black;
        }

        /* visited link */
        a:visited {
        color: black;
        }

        /* mouse over link */
        a:hover {
        color: hotpink;
        }

        /* selected link */
        a:active {
        color: hotpink;
        }

    </style>

</head>
<body>
    <div class="row p-1 mb-4 text-dark" style="background:rgb(237, 237, 237)">
        <div class="col-3  mt-2 d-flex justify-content-end">
        <svg width="72px" height="72px" viewBox="0 0 72 72" id="emoji" xmlns="http://www.w3.org/2000/svg">
            <g id="color">
              <circle cx="14.9688" cy="19.0938" r="3" fill="#92D3F5" stroke="none"/>
              <circle cx="14.9688" cy="19.0938" r="3" fill="#ea5a47" stroke="none" stroke-miterlimit="10" stroke-width="2"/>
            </g>
            <g id="hair"/>
            <g id="skin">
              <circle cx="39.9688" cy="11.0938" r="3" fill="#FCEA2B" stroke="none"/>
              <polygon fill="#FCEA2B" stroke="none" points="33.834,22 34.9951,24.7627 34.9688,24.1665"/>
              <path fill="#FCEA2B" stroke="none" d="M39.6807,17.916L25.083,18.9814L17.5,18.0156c0,0-1.3682,0.6846,0.4688,2.0274 c1.5458,1.1308,5.3251,2.541,7.3652,2.541c3.5,0,6.333-2.0835,7.668-0.3623c0.7666,0.9887,1.9931,2.541,1.9931,2.541L35.1357,28 l-3,7l-2,6l-10.1738,4l0.4238,3l13.75-2l3.5616-5.3418L37.1357,42v7l-6.1669,9l3,2l8.1669-9l3-13c0,0,1.0909-13.1929,1.2706-16 c0.1357-2.125,4.7255,0.146,8.1425,1.0625C56.4219,23.5649,59,24.6245,62,25.2495c2.1816,0.4546,2.8438-1.937,2.8438-1.937 l-18.2793-6.3726L39.6807,17.916z"/>
            </g>
            <g id="skin-shadow"/>
            <g id="line">
              <circle cx="39.9688" cy="11.0938" r="3" fill="none" stroke="#000000" stroke-miterlimit="10" stroke-width="2"/>
              <circle cx="14.9688" cy="19.0938" r="3" fill="none" stroke="#000000" stroke-miterlimit="10" stroke-width="2"/>
              <path fill="none" stroke="#000000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2" d="M40.1357,37l-4.8906,7.3359c-0.6103,0.9151-2,1.794-3.0888,1.9522l-9.7911,1.4238c-1.0888,0.1582-2.0752-0.3867-2.1914-1.2119 c-0.1162-0.8252,0.626-1.8291,1.6494-2.2314l6.4512-2.5372c1.0235-0.4023,2.1455-1.5849,2.4942-2.6289l0.7343-2.205 c0.3487-1.044,0.9873-2.7246,1.4209-3.7354l1.4239-3.3242c0.4336-1.0108,0.7509-2.7002,0.705-3.7549 c-0.0468-1.0542-0.3388-2.4038-0.6513-3C34.0889,22.4873,33.834,22,33.834,22"/>
              <path fill="none" stroke="#000000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2" d="M47.1357,28"/>
              <path fill="none" stroke="#000000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2" d="M37,42v5c0,1.0996-0.4414,2.7422-1.0625,3.6494l-3.8721,5.7012c-0.622,0.9072-0.4384,2.0996,0.3867,2.6494 c0.8252,0.5498,2.1124,0.334,2.8516-0.4814l5.4844-6.0372c0.7392-0.8154,1.5478-2.3584,1.7959-3.4306l2.1006-9.1016 c0.248-1.0722,0.4707-2.8486,0.4961-3.9482L45.4316,22"/>
              <path fill="none" stroke="#000000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2" d="M15.875,16l7.293,3.1973c1.0078,0.4414,2.6855,0.518,3.7295,0.1704l2.205-0.7354c1.044-0.3476,2.7959-0.6924,3.8926-0.7651 l11.0098-0.7344c1.0967-0.0727,2.8486,0.1519,3.8926,0.4995L64,23"/>
            </g>
          </svg>
          
        </div>
        <div class="col-7 mt-4 d-flex justify-content-start">
            <a href="https://zrkbrod.hr/" style="text-decoration: none;"><h2>ŽRK BROD</h2></a>
          </div>
        <div class="col-2 d-flex justify-content-end mr-3 mt-3">
              <button class="btn btn-outline-secondary mb-4" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasExample" aria-controls="offcanvasExample">
                Izbornik
                </button>
        </div>

    </div>
    
    
        
    @yield('content')
   
    <div class="offcanvas offcanvas-start" tabindex="-1" id="offcanvasExample" aria-labelledby="offcanvasExampleLabel">
        <div class="offcanvas-header">
            <h5 class="offcanvas-title" id="offcanvasExampleLabel">Izbornik</h5>
            <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
        </div>

        <div class="offcanvas-body">
            <div>
                @include('sidebar')
            </div>
            <hr>
            <div>
                <ul class="navbar-nav ms-auto p-3">
                                <!-- Authentication Links -->
                                @guest
                                    @if (Route::has('login'))
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                        </li>
                                    @endif

                                    @if (Route::has('register'))
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                        </li>
                                    @endif
                                @else
                                    <li class="nav-item dropdown">
                                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                            {{ Auth::user()->name }}
                                        </a>

                                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                            <a class="dropdown-item" href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                            document.getElementById('logout-form').submit();">
                                                {{ __('Logout') }}
                                            </a>

                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                                @csrf
                                            </form>
                                        </div>
                                    </li>
                                @endguest
                            </ul>
        
            </div>      

        </div>
    </div>


    <div class="container-fluid position-absolute bottom-0 end-0 text-dark" style="background: rgb(237, 237, 237)" >
        <footer class="text-end mt-2 text-muted" >
    
    
    
            <section >
              <div class="container text-center text-md-start ">
                <!-- Grid row -->
                
            </section>
            <!-- Section: Links  -->
          
            <!-- Copyright -->
            <div class="text-end p-2" style=" color: #000;">
                <div>2022</div>
            </div>
            <!-- Copyright -->
          </footer>
    </div>

            
 
    
</body>
</html>

@extends('layouts.app')

@section('content')

<div class="col py-3 px-3">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">

                <div class="p-5">
                    Dobrodošli na stranicu rukometnog turnira.
                </div>

                <div class="p-5">
                    U izborniku se mogu pogledati rasporedi ekipa po grupama.<br>
                    <em>U tijeku je izrada cjelokupnog rasporeda koji će biti objavljen u narednim danima.</em>
                </div>
            
            


        
            </div>
        </div>
    </div>
</div>
@endsection

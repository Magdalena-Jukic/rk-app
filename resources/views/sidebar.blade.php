
                <ul class="list-unstyled p-3">
                    <li class="nav-item">
                        <a href="/home" class="nav-link pl-0 text-nowrap"><span class="fs-5 d-sm-inline fw-bold">Home</span></a>
                    </li>
                    <li class="nav-item has-submenu">
                        <a href="#collapseYears" class="nav-link pl-0 text-nowrap" data-bs-toggle="collapse">
                            <span class="fw-bold d-md-inline">Godina</span>
                        </a>
                    
                        <ul class="submenu collapse list-unstyled ps-3" id="collapseYears">
                            @foreach($years as $year)
                            <li class="nav-item has-submenu">
                                <a href="#collapseYear{{$year->id}}" class="nav-link pl-0 text-nowrap" data-bs-toggle="collapse">
                                    <span class= "d-md-inline">{{$year->year}}</span>
                                </a>
                                
                                <ul class="submenu collapse list-unstyled ps-3" id="collapseYear{{$year->id}}">
                                
                                    <li class="nav-item">
                                        <a href="#collapseGeneration{{$year->id}}" class="nav-link pl-0 text-nowrap" data-bs-toggle="collapse">
                                            <span class= "fw-bold d-md-inline">Godišta</span>    
                                        </a>
                                        @foreach($generations as $generation)
                                        <ul class="submenu collapse list-unstyled ps-3" id="collapseGeneration{{$year->id}}">
                                            <li><a class="nav-link" href="/{{$year->year}}/generations/{{$generation->id}}">{{$generation->generation}}</a></li>
                                        </ul>
                                        @endforeach
                                    </li>
                                        
                                </ul>
                                
                            </li>
                            @endforeach
                        </ul>
                    </li>
                    <li class="nav-item has-submenu">
                        <a href="#collapseSportsCourt" class="nav-link pl-0 text-nowrap" data-bs-toggle="collapse">
                            <span class= " fw-bold d-md-inline">Dvorana</span>
                        </a>
                        
                        <ul class="submenu collapse list-unstyled ps-3" id="collapseSportsCourt">                     
                            @foreach($sports_courts as $sc)                                                
                            <li><a class="nav-link" href="/sports_court/{{$sc->name}}/{{$sc->id}}">{{$sc->name}}</a></li>    
                            @endforeach                                             
                         
                        </ul>

                    </li>

                </ul>
                           
                
                
            
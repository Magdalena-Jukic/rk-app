@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

            <div class="p-5">
                <form action="/{{$year}}/addTeam/{{$generation}}" method="get">
                    <label for="num">Broj ekipa:</label>
                    <input type="number" name="number">

                    <input type="submit" value="Dodaj" name="btn_one" class="btn btn-success">

                </form>
            </div>   

        <?php

            if(isset($_GET['btn_one'])){
                $num = $_REQUEST['number'];
                if($num >= 3){
                    if($num == 6 || $num == 8 || $num == 10){?>
                        <form action="/{{$year}}/{{$generation}}/team/store" method="POST" enctype="multipart/form-data">
                            @csrf
                            @method('POST')
                            
                            
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Klub</th>
                                    <th scope="col">Grupa</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php for($i = 1; $i <= ($num/2); $i++){ ?>
                                
                                    <tr>
                                    <th>{{$i}}</th>
                                    <td>
                                    
                                    <select name="club_name[]" id="club" class="form-select">
                                        <option value=""></option>
                                        @foreach($clubs as $club)
                                            <option value="{{$club->id}}" class="form-control">{{$club->prefix}} {{$club->name}}</option> 
                                        @endforeach
                                    </select>
                                    
                                    </td>
                                    <td>

                                        <select name="group[]" id="group" class="form-select">
                                            <option value=""></option>
                                            @foreach($groups as $group)
                                                <option value="{{$group->id}}" class="form-control">{{$group->group}}</option> 
                                            @endforeach
                                        </select>

                                    </td>
                                    
                                    </tr>
                                
                                <?php } ?>
                                </tbody>
                            </table>

                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Klub</th>
                                    <th scope="col">Grupa</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php for($i = 1; $i <= ($num/2); $i++){ ?>
                                
                                    <tr>
                                    <th>{{$i}}</th>
                                    <td>
                                        <select name="club_name[]" id="club" class="form-select">
                                            <option value=""></option>
                                            @foreach($clubs as $club)
                                                <option value="{{$club->id}}" class="form-control">{{$club->prefix}} {{$club->name}}</option> 
                                            @endforeach
                                        </select>
                                    </td>
                                    <td>

                                        <select name="group[]" id="group" class="form-select">
                                            <option value=""></option>
                                            @foreach($groups as $group)
                                                <option value="{{$group->id}}" class="form-control">{{$group->group}}</option> 
                                            @endforeach
                                        </select>

                                    </td>
                                    
                                    </tr>
                                
                                <?php } ?>
                                </tbody>
                            </table>
                        
                            <input type="submit" value="Spremi" class="btn btn-success">
                        </form>
                    <?php } elseif($num == 7){?>
                        <form action="/{{$year}}/{{$generation}}/team/store" method="POST" enctype="multipart/form-data">
                            @csrf
                            @method('POST')
                                    
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Klub</th>
                                            <th scope="col">Grupa</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php for($i = 1; $i <= ($num/2); $i++){ ?>
                                        
                                            <tr>
                                            <th>{{$i}}</th>
                                            <td>
                                                <select name="club_name[]" id="club" class="form-select">
                                                    <option value=""></option>
                                                    @foreach($clubs as $club)
                                                        <option value="{{$club->id}}" class="form-control">{{$club->prefix}} {{$club->name}}</option> 
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td>

                                                <select name="group[]" id="group" class="form-select">
                                                    <option value=""></option>
                                                    @foreach($groups as $group)
                                                        <option value="{{$group->id}}" class="form-control">{{$group->group}}</option> 
                                                    @endforeach
                                                </select>

                                            </td>
                                            
                                            </tr>
                                        
                                        <?php } ?>
                                        </tbody>
                                    </table>

                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Klub</th>
                                            <th scope="col">Grupa</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php for($i = 1; $i <= ($num/2)+1; $i++){ ?>
                                        
                                            <tr>
                                            <th>{{$i}}</th>
                                            <td>
                                                <select name="club_name[]" id="club" class="form-select">
                                                    <option value=""></option>
                                                    @foreach($clubs as $club)
                                                        <option value="{{$club->id}}" class="form-control">{{$club->prefix}} {{$club->name}}</option> 
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td>

                                                <select name="group[]" id="group" class="form-select">
                                                    <option value=""></option>
                                                    @foreach($groups as $group)
                                                        <option value="{{$group->id}}" class="form-control">{{$group->group}}</option> 
                                                    @endforeach
                                                </select>

                                            </td>
                                            
                                            </tr>
                                        
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                
                                    <input type="submit" value="Spremi" class="btn btn-success">
                                </form>
                    <?php } elseif($num == 9 || $num == 12 || $num == 15){?>   
                        <form action="/{{$year}}/{{$generation}}/team/store" method="POST" enctype="multipart/form-data">
                            @csrf
                            @method('POST')
                                    
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Klub</th>
                                            <th scope="col">Grupa</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php for($i = 1; $i <= ($num/3); $i++){ ?>
                                        
                                            <tr>
                                            <th>{{$i}}</th>
                                            <td>
                                                <select name="club_name[]" id="club" class="form-select">
                                                    <option value=""></option>
                                                    @foreach($clubs as $club)
                                                        <option value="{{$club->id}}" class="form-control">{{$club->prefix}} {{$club->name}}</option> 
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td>

                                                <select name="group[]" id="group" class="form-select">
                                                    <option value=""></option>
                                                    @foreach($groups as $group)
                                                        <option value="{{$group->id}}" class="form-control">{{$group->group}}</option> 
                                                    @endforeach
                                                </select>

                                            </td>
                                           
                                            </tr>
                                        
                                        <?php } ?>
                                        </tbody>
                                    </table>

                                    
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Klub</th>
                                            <th scope="col">Grupa</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php for($i = 1; $i <= ($num/3); $i++){ ?>
                                        
                                            <tr>
                                            <th>{{$i}}</th>
                                            <td>
                                                <select name="club_name[]" id="club" class="form-select">
                                                    <option value=""></option>
                                                    @foreach($clubs as $club)
                                                        <option value="{{$club->id}}" class="form-control">{{$club->prefix}} {{$club->name}}</option> 
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td>

                                                <select name="group[]" id="group" class="form-select">
                                                    <option value=""></option>
                                                    @foreach($groups as $group)
                                                        <option value="{{$group->id}}" class="form-control">{{$group->group}}</option> 
                                                    @endforeach
                                                </select>

                                            </td>
                                            
                                            </tr>
                                        
                                        <?php } ?>
                                        </tbody>
                                    </table>

                                    
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Klub</th>
                                            <th scope="col">Grupa</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php for($i = 1; $i <= ($num/3); $i++){ ?>
                                        
                                            <tr>
                                            <th>{{$i}}</th>
                                            <td>
                                                <select name="club_name[]" id="club" class="form-select">
                                                    <option value=""></option>
                                                    @foreach($clubs as $club)
                                                        <option value="{{$club->id}}" class="form-control">{{$club->prefix}} {{$club->name}}</option> 
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td>

                                                <select name="group[]" id="group" class="form-select">
                                                    <option value=""></option>
                                                    @foreach($groups as $group)
                                                        <option value="{{$group->id}}" class="form-control">{{$group->group}}</option> 
                                                    @endforeach
                                                </select>

                                            </td>
                                            
                                            </tr>
                                        
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                
                                    <input type="submit" value="Spremi" class="btn btn-success">
                                </form>     
                    <?php } elseif($num == 11 || $num == 13 || $num == 14){?>
                                <form action="/{{$year}}/{{$generation}}/team/store" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    @method('POST')
                                    
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Klub</th>
                                            <th scope="col">Grupa</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php for($i = 1; $i <= round($num/3); $i++){ ?>
                                        
                                            <tr>
                                            <th>{{$i}}</th>
                                            <td>
                                                <select name="club_name[]" id="club" class="form-select">
                                                    <option value=""></option>
                                                    @foreach($clubs as $club)
                                                        <option value="{{$club->id}}" class="form-control">{{$club->prefix}} {{$club->name}}</option> 
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td>

                                                <select name="group[]" id="group" class="form-select">
                                                    <option value=""></option>
                                                    @foreach($groups as $group)
                                                        <option value="{{$group->id}}" class="form-control">{{$group->group}}</option> 
                                                    @endforeach
                                                </select>

                                            </td>
                                            
                                            </tr>
                                        
                                        <?php } ?>
                                        </tbody>
                                    </table>

                                    
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Klub</th>
                                            <th scope="col">Grupa</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php for($i = 1; $i <= ($num/3); $i++){ ?>
                                        
                                            <tr>
                                            <th>{{$i}}</th>
                                            <td>
                                                <select name="club_name[]" id="club" class="form-select">
                                                    <option value=""></option>
                                                    @foreach($clubs as $club)
                                                        <option value="{{$club->id}}" class="form-control">{{$club->prefix}} {{$club->name}}</option> 
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td>

                                                <select name="group[]" id="group" class="form-select">
                                                    <option value=""></option>
                                                    @foreach($groups as $group)
                                                        <option value="{{$group->id}}" class="form-control">{{$group->group}}</option> 
                                                    @endforeach
                                                </select>

                                            </td>
                                            
                                            </tr>
                                        
                                        <?php } ?>
                                        </tbody>
                                    </table>

                                    
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Klub</th>
                                            <th scope="col">Grupa</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php for($i = 1; $i <= ($num/3)+1; $i++){ ?>
                                        
                                            <tr>
                                            <th>{{$i}}</th>
                                            <td>
                                                <select name="club_name[]" id="club" class="form-select">
                                                    <option value=""></option>
                                                    @foreach($clubs as $club)
                                                        <option value="{{$club->id}}" class="form-control">{{$club->prefix}} {{$club->name}}</option> 
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td>

                                                <select name="group[]" id="group" class="form-select">
                                                    <option value=""></option>
                                                    @foreach($groups as $group)
                                                        <option value="{{$group->id}}" class="form-control">{{$group->group}}</option> 
                                                    @endforeach
                                                </select>

                                            </td>
                        
                                            </tr>
                                        
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                
                                    <input type="submit" value="Spremi" class="btn btn-success">
                                </form>     
                    <?php } else{ ?> 
                            <form action="/{{$year}}/{{$generation}}/team/store" method="POST" enctype="multipart/form-data">
                                @csrf
                                @method('POST')
                                
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Klub</th>
                                            <th scope="col">Grupa</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php for($i = 1; $i <= $num; $i++){ ?>
                                        
                                            <tr>
                                            <th>{{$i}}</th>
                                            <td>
                                                <select name="club_name[]" id="club" class="form-select">
                                                    <option value=""></option>
                                                    @foreach($clubs as $club)
                                                        <option value="{{$club->id}}" class="form-control">{{$club->prefix}} {{$club->name}}</option> 
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td>

                                                <select name="group[]" id="group" class="form-select">
                                                    <option value=""></option>
                                                    @foreach($groups as $group)
                                                        <option value="{{$group->id}}" class="form-control">{{$group->group}}</option> 
                                                    @endforeach
                                                </select>

                                            </td>
                                            </tr>
                                        
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                
                                    <input type="submit" value="Spremi" class="btn btn-success">

                                </form>     
                        <?php } ?>
                    
                <?php } ?>
                
            <?php } ?>
               
    
        </div>
    </div>
</div>
@endsection

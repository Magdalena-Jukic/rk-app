@extends('layouts.app')

@section('content')

<div class="col py-3 px-3">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">

            <div class="p-5">
                <form action="/test/number" method="get">
                    <label for="num">Broj ekipa:</label>
                    <input type="number" name="number">

                    <input type="submit" value="spremi" name="btn_one" class="btn btn-success">

                </form>
            </div>
            
            


        <?php

            if(isset($_GET['btn_one'])){
                $num = $_REQUEST['number'];
                if($num >= 3){
                    if($num == 6 || $num == 8 || $num == 10){?>
                        <form action="/test/store" method="get">
                            
                            <div>Groupa A</div>
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Klub</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php for($i = 1; $i <= ($num/2); $i++){ ?>
                                
                                    <tr>
                                    <th>{{$i}}</th>
                                    <td><input type="text" name="club_name[]"></td>
                                    </tr>
                                
                                <?php } ?>
                                </tbody>
                            </table>

                            <div>Groupa B</div>
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Klub</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php for($i = 1; $i <= ($num/2); $i++){ ?>
                                
                                    <tr>
                                    <th>{{$i}}</th>
                                    <td><input type="text" name="club_name[]"></td>
                                    </tr>
                                
                                <?php } ?>
                                </tbody>
                            </table>
                        
                            <button type="submit">Spremi</button>
                        </form>
                    <?php } elseif($num == 7){?>
                        <form action="/test/store" method="get">
                                    <div>Groupa A</div>
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Klub</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php for($i = 1; $i <= ($num/2); $i++){ ?>
                                        
                                            <tr>
                                            <th>{{$i}}</th>
                                            <td><input type="text" name="club_name[]"></td>
                                            </tr>
                                        
                                        <?php } ?>
                                        </tbody>
                                    </table>

                                    <div>Groupa B</div>
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Klub</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php for($i = 1; $i <= ($num/2)+1; $i++){ ?>
                                        
                                            <tr>
                                            <th>{{$i}}</th>
                                            <td><input type="text" name="club_name[]"></td>
                                            </tr>
                                        
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                
                                    <button type="submit">Spremi</button>
                                </form>
                    <?php } elseif($num == 9 || $num == 12 || $num == 15){?>   
                        <form action="/test/store" method="get">
                                    <div>Groupa A</div>
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Klub</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php for($i = 1; $i <= ($num/3); $i++){ ?>
                                        
                                            <tr>
                                            <th>{{$i}}</th>
                                            <td><input type="text" name="club_name[]"></td>
                                            </tr>
                                        
                                        <?php } ?>
                                        </tbody>
                                    </table>

                                    <div>Groupa B</div>
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Klub</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php for($i = 1; $i <= ($num/3); $i++){ ?>
                                        
                                            <tr>
                                            <th>{{$i}}</th>
                                            <td><input type="text" name="club_name[]"></td>
                                            </tr>
                                        
                                        <?php } ?>
                                        </tbody>
                                    </table>

                                    <div>Groupa C</div>
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Klub</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php for($i = 1; $i <= ($num/3); $i++){ ?>
                                        
                                            <tr>
                                            <th>{{$i}}</th>
                                            <td><input type="text" name="club_name[]"></td>
                                            </tr>
                                        
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                
                                    <button type="submit">Spremi</button>
                                </form>     
                    <?php } elseif($num == 11 || $num == 13 || $num == 14){?>
                                <form action="/test/store" method="get">
                                    <div>Groupa A</div>
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Klub</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php for($i = 1; $i <= round($num/3); $i++){ ?>
                                        
                                            <tr>
                                            <th>{{$i}}</th>
                                            <td><input type="text" name="club_name[]"></td>
                                            </tr>
                                        
                                        <?php } ?>
                                        </tbody>
                                    </table>

                                    <div>Groupa B</div>
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Klub</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php for($i = 1; $i <= ($num/3); $i++){ ?>
                                        
                                            <tr>
                                            <th>{{$i}}</th>
                                            <td><input type="text" name="club_name[]"></td>
                                            </tr>
                                        
                                        <?php } ?>
                                        </tbody>
                                    </table>

                                    <div>Groupa C</div>
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Klub</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php for($i = 1; $i <= ($num/3)+1; $i++){ ?>
                                        
                                            <tr>
                                            <th>{{$i}}</th>
                                            <td><input type="text" name="club_name[]"></td>
                                            </tr>
                                        
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                
                                    <button type="submit">Spremi</button>
                                </form>     
                    <?php } else{ ?> 
                            <form action="/test/store" method="get">
                                <div>Groupa A</div>
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Klub</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php for($i = 1; $i <= $num; $i++){ ?>
                                        
                                            <tr>
                                            <th>{{$i}}</th>
                                            <td><input type="text" name="club_name[]"></td>
                                            </tr>
                                        
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                
                                    <button type="submit" class="btn btn-success">Spremi</button>
                                </form>     
                        <?php } ?>
                    
                <?php } ?>
                
            <?php } ?>
 
            </div>
        </div>
    </div>
</div>
@endsection

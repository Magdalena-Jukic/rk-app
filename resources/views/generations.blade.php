@extends('layouts.app')

@section('content')
    
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

       
            <div>
                <h3>Godište {{$generation->generation}}</h3>
            </div>
            

            <ul class="nav nav-pills" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active " data-bs-toggle="pill" href="#teams">Popis ekipa</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-bs-toggle="pill" href="#groups">Grupe</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-bs-toggle="pill" href="#scheduler">Raspored</a>
                </li>
            </ul>


            <div class="tab-content">
                <div id="teams" class="container tab-pane active"><br>
                    <div class="overflow-auto p-3 mb-3 mb-md-0 mr-md-3 " style="max-width: 1000px; max-height: 500px; height: 500px;">
                        <table class="table table-bordered test text-center">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th scope="col">Ekipe</th>
                                    <th scope="col">Grad</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data as $row)
                                    
                                    <tr>
                                        
                                        <td>{{$row->prefix}} {{$row->name}}</td>
                                        <td>{{$row->city}}</td>
                                    </tr>
                                @endforeach
                                
                            </tbody>
                        </table>
                    </div>
                </div>

                <div id="groups" class="container tab-pane fade "><br>
                    <div class="overflow-auto p-3 mb-3 mb-md-0 mr-md-3 " style="max-width: 1000px; max-height: 500px; height: 500px;">
                        <div class="row">
                            
                        @if($data != [])
                        
                            <table class="table table-bordered mx-auto testgroupA text-center" style="width: 200px;">
                                <thead>
                                            
                                    <tr>
                                        <th>#</th>
                                        <th scope="col">Grupa A</th>
                                    </tr>
                                            
                                </thead>
                                        
                                <tbody>
                                    @foreach($data as $row)
                                        @if($row->group_id == 1)
                                        
                                        <tr>
                                                                                                                 
                                            <td>{{$row->prefix}} {{$row->name}}</td>
                                                                                            
                                        </tr>
                                        @endif
                                    @endforeach  
                                </tbody>
                                
                            </table> 
                            
                            <table class="table table-bordered mx-auto testgroupB text-center" style="width: 200px;">
                                    
                                <thead>
                                        
                                    <tr>
                                        <th>#</th>
                                        <th scope="col">Grupa B</th>
                                        
                                    </tr>                                            
                                            
                                </thead>
                                        
                                <tbody>
                                    @foreach($data as $row)
                                        @if($row->group_id == 2)
                                        <tr>
                                                                           
                                            <td>{{$row->prefix}} {{$row->name}}</td>
                                                                                            
                                        </tr>
                                        @endif
                                    @endforeach
                                              
                                </tbody>
                                          
                            </table> 
                                  
                                    
                        @endif         
                              
                        </div>
                    </div>
                </div>

                <div id="scheduler" class="container tab-pane fade"><br>
                    <div class="overflow-auto p-3 mb-3 mb-md-0 mr-md-3 " style="max-width: 1000px; max-height: 500px; height: 500px;">
                       
                            <h5>Grupa A</h5>
                                <table class="table table-bordered text-center testschedulerA" style="font-size: 15px;">
                                    <thead>
                                        <tr>
                                            
                                            <th>#</th>     
                                            <th scope="col">Br.Ut.</th>                                      
                                            <th scope="col">Dom.</th>
                                            <th scope="col">Gost</th>
                                            <th scope="col">Teren</th>
                                            <th scope="col">Vr.</th>                                            
                                            <th scope="col">Dan</th>                                          
                                        
                                        </tr>
                                    </thead>
                                    
                                    <tbody> 
                                    @foreach($games as $game)
                                    @foreach($game->teamOne as $t_1)
                                    @foreach($game->teamTwo as $t_2)
                                    @if($t_1->generation->id == $generation->id && $t_2->generation->id == $generation->id && $t_1->group->id == 1 && $t_2->group->id == 1)
                                    
                                    <tr> 
                                            
                                        <td>{{$game->game_number}}</td>
                                        <td>{{$t_1->club->name}} {{$t_1->generation->generation}}</td>
                                       
                                        <td>{{$t_2->club->name}} {{$t_2->generation->generation}}</td>
                                       
                                        <td>{{$game->sports_court->name ?? '-'}}</td>
                                        <td>{{$game->time_meet->time ?? '-'}}</td>
                                        <td>{{$game->day_meet->day ?? '-'}}</td>
                                    </tr>
                                    @endif
                                    @endforeach
                                    @endforeach
                                    @endforeach                                                           
                                    </tbody>
                                    
                                </table>
                        
                    
                            <h5>Grupa B</h5>
                                <table class="table table-bordered text-center testschedulerB" style="font-size: 15px;">
                                    <thead>
                                        <tr>
                                            <th>#</th>   
                                            <th scope="col">Br.Ut.</th>                                        
                                            <th scope="col">Dom.</th>
                                            <th scope="col">Gost</th>
                                            <th scope="col">Vr.</th>
                                            <th scope="col">Teren</th>
                                            <th scope="col">Dan</th>                                          
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($games as $game)
                                    @foreach($game->teamOne as $t_1)
                                    @foreach($game->teamTwo as $t_2)
                                    @if($t_1->generation->id == $generation->id && $t_2->generation->id == $generation->id && $t_1->group->id == 2 && $t_2->group->id == 2)
                                    
                                    <tr> 
                                            
                                        <td>{{$game->game_number}}</td>
                                        <td>{{$t_1->club->name}} {{$t_1->generation->generation}}</td>
                                       
                                        <td>{{$t_2->club->name}} {{$t_2->generation->generation}}</td>
                                       
                                        <td>{{$game->sports_court->name ?? '-'}}</td>
                                        <td>{{$game->time_meet->time ?? '-'}}</td>
                                        <td>{{$game->day_meet->day ?? '-'}}</td>
                                    </tr>
                                    @endif
                                    @endforeach
                                    @endforeach
                                    @endforeach    
                                    
                                    </tbody>
                                </table>
                        
                    </div>
                </div>
            </div>

            @guest
            @else
            <div class="pt-4">
                <a class="btn btn-outline-secondary" href="/{{$year}}/addTeam/{{$generation->id}}"  role="button">
                    Dodaj ekipu
                </a>
            </div>
            
            @endguest
            
        
        </div>
    </div>
</div>
<script>
    var addNumeration = function(cl){
    var table = document.querySelector('table.' + cl)
    var trs = table.querySelectorAll('tr')
    var counter = 1
  
    Array.prototype.forEach.call(trs, function(x,i){
        var firstChild = x.children[0]
        

        if (firstChild.tagName === 'TD') {
            var cell = document.createElement('td')
            cell.textContent = counter ++
            x.insertBefore(cell,firstChild)
        } else {
            firstChild.setAttribute('colspan',1)
            

        }
    })
    }

    addNumeration("test")
    addNumeration("testgroupA")
    addNumeration("testgroupB")
    addNumeration("testschedulerA")
    addNumeration("testschedulerB")


</script>
@endsection

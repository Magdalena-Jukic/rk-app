@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

        
            <h5>{{$sports_court_name}}</h5>
            
            <div class="overflow-auto p-3 mb-3 mb-md-0 mr-md-3 " style="max-width: 1000px; max-height: 500px; height: 500px;">
                <div>
                    <table class="table table-bordered  text-center test " style="font-size: 15px;">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th scope="col">Br.Ut.</th>
                                            <th scope="col"></th>
                                            <th scope="col"></th>
                                            <th scope="col">Dom.</th>
                                            <th scope="col">Gost</th>
                                            <th scope="col">Vr.</th>
                                            <th scope="col">Dan</th>                                          
                                            <th scope="col">Rez</th>                                          
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($games as $game)
                                        @if($game->sports_court->name == $sports_court_name)
                                        @foreach($game->teamOne as $t_1)
                                        @foreach($game->teamTwo as $t_2)
                                        @if($t_1->generation->id == $t_2->generation->id && $t_1->group->id == $t_2->group->id)
                                        <tr>
                                            
                                            <td>{{$game->game_number}}</td>
                                            <td>{{$t_1->generation->generation}}</td>
                                            <td>{{$t_2->group->group}}</td>
                                            <td>{{$t_1->club->name}}</td>
                                            <td>{{$t_2->club->name}}</td>
                                            <td>{{$game->time_meet->time ?? '-'}}</td>
                                            <td>{{$game->day_meet->day ?? '-'}}</td>  
                                            <td></td>                                        
                                        </tr>
                                        @endif
                                        @endforeach
                                        @endforeach
                                        @endif
                                        @endforeach 
                                        
                                    </tbody>
                    </table>

                    
                </div>
            <div>
            
           
        
        </div>
    </div>
</div>

<script>
    var addNumeration = function(cl){
    var table = document.querySelector('table.' + cl)
    var trs = table.querySelectorAll('tr')
    var counter = 1
  
    Array.prototype.forEach.call(trs, function(x,i){
        var firstChild = x.children[0]
        

        if (firstChild.tagName === 'TD') {
            var cell = document.createElement('td')
            cell.textContent = counter ++
            x.insertBefore(cell,firstChild)
        } else {
            //firstChild.setAttribute('colspan',1)
            

        }
  })
}

addNumeration("test")
</script>
@endsection

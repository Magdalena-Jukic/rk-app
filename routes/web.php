<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Auth::routes();

Route::get('/', function () {
    return view('home'); //odmah se pokaze home.blade
});

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');// za login i register

//promjeniti!
Route::get('/{year}/generations/{generation}', [App\Http\Controllers\TournamentsController::class, 'show']);

Route::view('/generations/{generation}', 'generations'); //->treba ovako 

//Route::view('/addClub/{generation}', [App\Http\Controllers\ClubsController::class, 'show']);
//Route::get('/addClub/{generation}/{gender}', [App\Http\Controllers\ClubsController::class, 'show']);


//Route::get('/addClub', [App\Http\Controllers\TournamentsController::class, 'table']);



//testiranje
//Route::get('/test/number', [App\Http\Controllers\HomeController::class, 'test']);
//Route::get('/test', [App\Http\Controllers\HomeController::class, 'indextest']);


//Route::get('/test/store', [App\Http\Controllers\HomeController::class, 'testStore']);


//Route::view('/test', 'test');

Route::get('/{year}/addTeam/{generation}', [App\Http\Controllers\TeamsController::class, 'makeTeam']);
Route::post('/{year}/{generation}/team/store', [App\Http\Controllers\TeamsController::class, 'storeTeam']);

Route::get('/sports_court/{sports_court_name}/{sports_court_id}', [App\Http\Controllers\GamesController::class, 'show']);




//Route::get('/{year}/addTeam/{generation}', [App\Http\Controllers\TeamsController::class, 'storeTeam']);
//Route::get('/{year}/addTeam/{generation}', [App\Http\Controllers\TeamsController::class, 'show']);



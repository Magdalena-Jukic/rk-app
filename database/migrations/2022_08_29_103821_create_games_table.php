<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('games', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('game_number');
            $table->bigInteger('first_team_id');
            $table->bigInteger('second_team_id');
            $table->bigInteger('time_meet_id');
            $table->bigInteger('day_meet_id');
            $table->bigInteger('sports_court_id');
            $table->bigInteger('first_team_result_id');
            $table->bigInteger('second_team_result_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('games');
    }
};

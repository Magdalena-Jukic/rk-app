<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\Year;
use App\Models\Generation;
use App\Models\Gender;
use App\Models\SportsCourt;
use App\Models\Group;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        view()->composer('sidebar', function($view){
            $view->with('years', Year::orderBy("id", "desc")->get());
        });

        view()->composer('sidebar', function($view){
            $view->with('generations', Generation::orderBy("id", "asc")->get());
        });

        view()->composer('sidebar', function($view){
            $view->with('sports_courts', SportsCourt::all());
        });

        view()->composer('generations', function($view){
            $view->with('groups', Group::all());
        });

        

        /*view()->composer('sidebar', function($view){
            $view->with('gender', Gender::all());
        });*/
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;



class Game extends Model
{
    use HasFactory;
    protected $fillable = [
        'game_number',
        'first_team_id',
        'second_team_id',
        'time_meet_id',
        'day_meet_id',
        'sports_court_id',

    ];

    //za ispis na raspored!!
    public function time_meet(){
        return $this->belongsTo(TimeMeet::class, 'time_meet_id', 'id' );
    }

    public function day_meet(){
        return $this->belongsTo(DayMeet::class, 'day_meet_id', 'id' );
    }
    public function sports_court(){
        return $this->belongsTo(SportsCourt::class, 'sports_court_id', 'id' );
    }

    public function teamOne(){
        return $this->hasMany(Team::class, 'id', 'first_team_id');
    }

    public function teamTwo(){
        return $this->hasMany(Team::class, 'id', 'second_team_id');
    }

    
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;




class Team extends Model
{
    use HasFactory;
    protected $fillable = [
        'club_id',
        'generation_id',
        'year_id', 
        'group_id',
    ];

   public function club(){
    return $this->hasOne(Club::class, 'id', 'club_id');
   }

   public function generation(){
    return $this->hasOne(Generation::class, 'id', 'generation_id');
   }

   public function group(){
    return $this->hasOne(Group::class, 'id', 'group_id');
   }
}

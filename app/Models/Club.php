<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Club extends Model
{
    use HasFactory;
    protected $fillable = [
        'prefix',
        'name',
    ];

    public function team(){
        return $this->hasMany(Team::class, 'club_id', 'id');
    }
}

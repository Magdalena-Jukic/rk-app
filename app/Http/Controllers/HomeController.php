<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Club;
use App\Models\Generation;
use App\Models\Gender;
use App\Models\Team;
use App\Models\Year;



class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        //$clubs = Club::all();
        //return view('home', ['clubs'=>$clubs]);
        return view('home');
    }

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Generation;
use App\Models\Gender;
use App\Models\Year;
use App\Models\Team;
use App\Models\Club;
use App\Models\Group;
use App\Models\Game;
use App\Models\SportsCourt;


class TournamentsController extends Controller
{

    //za tablica za svako godište!!
    public function show($year, $generation_id){

        $generation = Generation::find($generation_id);
        
    
        
        $yearAll = Year::all();
        //echo $yearAll;
        $year_id = 0;
        foreach($yearAll as $r){
            if($r->year == $year){

                $year_id = $r->id;
                //echo $year_id;
            }else{
                //echo "ne";
            }

        }
        
        //echo $year_id;
        //var_dump($year_id);
        
        /*foreach($yearAll as $r){
            var_dump($r->year);
            var_dump($r->id);
            
            if($r->year == (integer)$year){
                $year_id = $r->id;
            }
            
        }*/
        //echo $year_id;
        //var_dump($year);
        //$year = (integer)$year;
        //var_dump($year);
        //echo $year_id;
        
       // $k = new Team;
        //$k->year_id = 2;
        //var_dump($k->year_id);

        //$z= Team::all();
        //foreach($z as $r){
          //  var_dump($r->year_id);
        //}

        

        /*$data = Team::join('clubs', 'clubs.id', '=', 'teams.club_id' )
                    ->join('groups', 'groups.id', '=', 'teams.group_id')
                    ->join('generations', 'generations.id', '=', 'teams.generation_id')
                    ->join('years', 'years.id', '=', 'teams.year_id')
                    ->get(['teams.id','clubs.prefix','clubs.name', 'clubs.city', 'groups.group', 'generations.generation', 'years.year']); */ 
                
       
       $data = Team::join('clubs', 'clubs.id', '=', 'teams.club_id' )//ispis grupa i opci ispis
                    ->join('groups', 'groups.id', '=', 'teams.group_id')
                    ->join('generations', 'generations.id','=', 'teams.generation_id')
                    ->join('years', 'years.id','=', 'teams.year_id')
                    ->where('teams.generation_id', '=', $generation_id)
                    ->where('teams.year_id', '=',  $year_id)
                    ->get(['teams.group_id', 'clubs.prefix','clubs.name', 'clubs.city','teams.club_id']); 

        /*$scheduler = Game::join('teams as first_team', 'first_team.id', '=', 'games.first_team_id')
                        ->join('teams as second_team', 'second_team.id', '=', 'games.second_team_id')
                        ->join('time_meets', 'time_meets.id', '=', 'games.time_meet_id')
                        ->join('day_meets', 'day_meets.id', '=', 'games.day_meet_id')
                        ->join('sports_courts', 'sports_courts.id', '=', 'games.sports_court_id')
                        ->get(['first_team.id as first_id', 'second_team.id as second_id', 'sports_courts.short_name', 'time_meets.time', 'day_meets.day']);*/
        //$scheduler = Game::all();

        $games = Game::all();
        
       
        //return view('/generations', ['generation' => $generation, 'year' => $year, 'data' => $data, 'scheduler' => $scheduler]);
        return view('/generations', ['generation' => $generation, 'year' => $year, 'data' => $data, 'games' => $games]);
             
    }

    

    




    
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Club;
use App\Models\Generation;
use App\Models\Gender;
use App\Models\Group;
use App\Models\Team;
use App\Models\Year;



class TeamsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');

    }
    public function makeTeam($year, $generation_id){ 
        
        $clubs = Club::all();
        $groups = Group::all();

        return view('/addTeam', ['clubs'=> $clubs, 'groups'=>$groups, 'year'=>$year, 'generation'=>$generation_id]);
    }
    
    public function storeTeam(Request $request, $year, $generation_id){
        //echo "godina: ".$year. " id generacije: ".$generation_id;
        //echo "teamStore function";
        //$team = new Team; //ovo zelimo spremiti


        $AllYears = Year::all();
        foreach($AllYears as $y){
            if($y->year == $year){
                $year_id = $y->id; // imamo id godine!
            }
        }

        //imamo id generacije!

       //spremanje svega toga!!
        $row=$request->club_name;
        $teams=[];
    
        foreach($request->club_name as $key => $value){
            array_push($teams, [
                'club_id' => $request->club_name[$key],
                'group_id' => $request->group[$key],
                'generation_id' => $generation_id,
                'year_id' => $year_id,

            ]);    
        }
        Team::insert($teams);
        return redirect('/{{$year}}/generations/{{$generation}}');

    }

    public function show(){
        return view('addTeam');
    }
    

}
